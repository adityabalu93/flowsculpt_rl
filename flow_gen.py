import os
import numpy as np
import random
import matlab.engine

#Option to print untruncated np array
np.set_printoptions(threshold=np.inf)
#The start_matlab function returns a Python object, eng, which allows you to pass data and call functions executed by MATLAB.
eng = matlab.engine.start_matlab() 

def gen_flow(pillar_num): #generates one image
	flow_img = np.ones((100,12))
	pillar = matlab.int8(pillar_num)
	flow_img[:,:] = eng.Pillar_program_regen2_12100(pillar)
	print(flow_img)
	return flow_img
	
#if __name__ == '__main__':
    #gen_flow([13])