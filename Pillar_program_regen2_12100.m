function [ label, pillars ] = Pillar_program_regen2(pillars)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

mData = loadSingleVariableMATFile('mDataR.mat');

inlet = form_inlet('M');

label = pillar_program(mData,pillars,inlet);

end

function [ params_array ] = dist_to_index( dist_array )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
bins = 10;
npillars = size(dist_array,2)/bins;
x= linspace(0,1,bins*0.5)';

for n = 1:npillars
    
    mu_dist = dist_array((n-1)*bins + 1:n*bins - 0.5*bins)';
    
    sig_dist = dist_array((n)*bins - (0.5*bins) + 1:(n)*bins)';
    
    if mu_dist == zeros(bins*0.5,1)
        params_array(n,1:2) = [0,0];
        continue
    else
        mu_params = fit(x,mu_dist,'gauss1');
        sig_params = fit(x,sig_dist,'gauss1');
        %params_array(n,1:2) = round([mu_params.b1, sig_params.b1],3);
        params_array(n,1:2) = round([mu_params.b1, sig_params.b1]*1000)/1000;
        
    end

end

end

function [params] =  pillar_dict(pillar)

switch pillar
    case 0
        params = [0.0, 0.0, 0.0];
    case 1
        params = [0.0, 1.0, 0.375];
    case 2
        params = [0.875, 0.375, 0.0];
    case 3
        params = [0.750, 0.375, 0.0];
    case 4
        params = [0.625, 0.375, 0.0];
    case 5
        params = [0.500, 0.375, 0.0];
    case 6
        params = [0.375, 0.375, 0.0];
    case 7
        params = [0.250, 0.375, 0.0];
    case 8
        params = [0.125, 0.375, 0.0];
    case 9
        params = [0.000, 1.000, 0.500];
    case 10
        params = [0.875, 0.500, 0.0];
    case 11
        params = [0.750, 0.500, 0.0];
    case 12
        params = [0.625, 0.500, 0.0];
    case 13
        params = [0.500, 0.500, 0.0];
    case 14
        params = [0.375, 0.500, 0.0];
    case 15
        params = [0.250, 0.500, 0.0];
    case 16
        params = [0.125, 0.500, 0.0];
    case 17
        params = [0.000, 1.000, 0.625];
    case 18
        params = [0.875, 0.625, 0.0];
    case 19
        params = [0.750, 0.625, 0.0];
    case 20
        params = [0.625, 0.625, 0.0];
    case 21
        params = [0.500, 0.625, 0.0];
    case 22
        params = [0.375, 0.625, 0.0];
    case 23
        params = [0.250, 0.625, 0.0];
    case 24
        params = [0.125, 0.625, 0.0];
    case 25
        params = [0.000, 1.0, 0.750];
    case 26
        params = [0.875, 0.750, 0.0];
    case 27
        params = [0.750, 0.750, 0.0];
    case 28
        params = [0.625, 0.750, 0.0];
    case 29
        params = [0.500, 0.750, 0.0];
    case 30
        params = [0.375, 0.750, 0.0];        
    case 31
        params = [0.250, 0.750, 0.0];        
    case 32
        params = [0.125, 0.750, 0.0];  
end

end

function [pillar] = mData_dict(params)

%params(1) = round(params(1),1);
%params(1) = round(params(1)*10)/10;

if params == [0.0, 0.0]
    pillar = 0;
elseif params == [0.100, 0.375]
    pillar = 1;
elseif params == [0.200, 0.375]
    pillar = 2;
elseif params == [0.300, 0.375]
    pillar = 3;
elseif params == [0.400, 0.375]
    pillar = 4;
elseif params == [0.500, 0.375]
    pillar = 5;
elseif params == [0.600, 0.375]
    pillar = 6;
elseif params == [0.700, 0.375]
    pillar = 7;
elseif params == [0.800, 0.375]
    pillar = 8;
elseif params == [0.100, 0.500]
    pillar = 9;
elseif params == [0.200, 0.5]
    pillar = 10;
elseif params == [0.300, 0.5]
    pillar = 11;
elseif params == [0.400, 0.5]
    pillar = 12;
elseif params == [0.500, 0.500]
    pillar = 13;
elseif params == [0.600, 0.500]
    pillar = 14;
elseif params == [0.700, 0.500]
    pillar = 15;
elseif params == [0.800, 0.500]
    pillar = 16;
elseif params == [0.100, 0.625]
    pillar = 17;
elseif params == [0.200, 0.625]
    pillar = 18;
elseif params == [0.300, 0.625]
    pillar = 19;
elseif params == [0.400, 0.625]
    pillar = 20;
elseif params == [0.500, 0.625]
    pillar = 21;
elseif params == [0.600, 0.625]
    pillar = 22;
elseif params == [0.700, 0.625]
    pillar = 23;
elseif params == [0.800, 0.625]
    pillar = 24;
elseif params == [0.100, 0.750]
    pillar = 25;
elseif params == [0.200, 0.750]
    pillar = 26;
elseif params == [0.300, 0.750]
    pillar = 27;
elseif params == [0.400, 0.750]
    pillar = 28;
elseif params == [0.500, 0.750]
    pillar = 29;
elseif params == [0.600, 0.750]
    pillar = 30;       
elseif params == [0.700, 0.750]
    pillar = 31;      
elseif params == [0.800, 0.750]
    pillar = 32;
else
    warning('Not an anticipated pillar parameter set.');
end   

end

function [inlet] = form_inlet(inlet_type)

%% Create binary inlet image with zeros and ones, based on inlet type
Nx = 801;
Ny = 101;

switch inlet_type
    case 'M'
        channels = [0 0 1 0 0];
    case 'MR401'
        channels = [0 0 1 1 0];
    case 'MR402'
        channels = [0 0 1 0 1];
    case 'MR60'
        channels = [0 0 1 1 1];
    case 'MR80'
        channels = [0 1 1 1 1];
    case 'R1'
        channels = [0 0 0 1 0];
    case 'R2'
        channels = [0 0 0 0 1];
    case 'R40'
        channels = [0 0 0 1 1];
end

channels = [0 0 1 0 0];

inlet = repmat([channels(1)*ones(1,((Nx-1)*0.2)+1,1) channels(2)*...
    ones(1,(Nx-1)*0.2,1) channels(3)*ones(1,(Nx-1)*0.2,1) channels(4)*...
    ones(1,(Nx-1)*0.2,1) channels(5)*ones(1,(Nx-1)*0.2)],1,Ny);

end

function [ outlet ] = pillar_program(mData, seq, inlet)
%BlackBox uses Markov matrix cell data 'mData' and a vector sequence 'seq'
%to assemble the image of the displaced pixels at the outlet of a 
%microchannel with geometry corresponding to the chosen Markov matrices.  

%Dimensions of channel. Nx * Ny = size(mcell) is required.
Nx = 801;
Ny = 101;

%% Form final product Markov matrix

%separate sequence and channels from input 'x'
%seq = x(1:end);

%Determine size of sequence for product loop
N=size(seq,2);

%Begin markov product with first value from the sequence
mProd=mData{seq(1)};

%multiply through the sequence to form the final product markov matrix
for i = 2:N
    if seq(i)==0
        continue
    end
    mProd=mProd*mData{seq(i)};
end

%Enforce no-slip condition

%Reshape Nx x Ny image into 1D vector for multiplication

%% left-multiply Markov matrix by inlet image to form outlet image, reshape
outlet = reshape((inlet * mProd),Nx,Ny);   

%Zero out no-slip region
outlet(:,Ny) = zeros(Nx,1);

%Post processing

outlet = bpffilter1(outlet,15);

outlet = imresize(outlet,'bilinear','OutputSize',[100 12]);

outlet = mat2gray(outlet);

outlet = ceil(outlet - 0.2);

[w,h] = size(outlet);

%outlet = reshape(outlet,1,w*h);

end

function [f_img] = bpffilter1(img,sigmaf)

PQ1 = 2*size(img);

H1 = lpfilter('gaussian', PQ1(1), PQ1(2), sigmaf);

F = fft2(img, size(H1,1), size(H1,2));

f_img_f = H1.*F;

f_img = real(ifft2(f_img_f));

f_img = f_img(1:size(img,1),1:size(img,2));

end

%--------------------------------------------------------------------------

function H = lpfilter(type, M, N, D0, n)
%LPFILTER Computes frequency domain lowpass filters
%   H = LPFILTER(TYPE, M, N, D0, n) creates the transfer function of
%   a lowpass filter, H, of the specified TYPE and size (M-by-N).  To
%   view the filter as an image or mesh plot, it should be centered
%   using H = fftshift(H).

[U, V] = dftuv(M, N);

% Compute the distances D(U, V).
D = sqrt(U.^2 + V.^2);

% Begin fiter computations.
switch type
case 'ideal'
   H = double(D <=D0);
case 'btw'
   if nargin == 4
      n = 1;
   end
   H = 1./(1 + (D./D0).^(2*n));
case 'gaussian'
   H = exp(-(D.^2)./(2*(D0^2)));
otherwise
   error('Unknown filter type.')
end

end

function [U, V] = dftuv(M, N)
%DFTUV Computes meshgrid frequency matrices.
%   [U, V] = DFTUV(M, N) computes meshgrid frequency matrices U and
%   V. U and V are useful for computing frequency-domain filter 
%   functions that can be used with DFTFILT.  U and V are both M-by-N.

% Set up range of variables.
u = 0:(M-1);
v = 0:(N-1);

% Compute the indices for use in meshgrid
idx = find(u > M/2);
u(idx) = u(idx) - M;
idy = find(v > N/2);
v(idy) = v(idy) - N;

% Compute the meshgrid arrays
[V, U] = meshgrid(v, u);
end


function y = loadSingleVariableMATFile(filename)

foo = load(filename);

whichVariables = fieldnames(foo);

if numel(whichVariables) == 1
    
    y = foo.(whichVariables{1});
    
else
    
    % Do something else that defines y, or error
    
end

end