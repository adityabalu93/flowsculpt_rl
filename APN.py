#only using network for CPU version of RL
from __future__ import print_function
import argparse

import chainer
if chainer:
    try:
        print('Using Chainer\n')
    except:
        print('Chainer Not available', '')
import chainer.links as L
import chainer.functions as F
from chainer import training, function, link, initializers
from chainer import variable, cuda
from chainer.training import extensions
from chainer.training import extensions
from chainer.dataset import concat_examples
import chainer.computational_graph as c
import numpy as np
import random
#import load_data
np.set_printoptions(threshold=np.inf)

#gpu = 1
#chainer.cuda.get_device_from_id(gpu).use()


class CNN(chainer.Chain):

    def __init__(self):
        super(CNN, self).__init__()
        with self.init_scope():
            self.l1 = L.Convolution2D(None, 32, ksize = 4, stride = 2, pad = 1)
            self.b1 = L.BatchNormalization(32)
            self.l2 = L.Convolution2D(None, 64, ksize = 4, stride = 1,  pad = 1)
            self.b2 = L.BatchNormalization(64)
            self.fc1 = L.Linear(None, 128)
        

    def __call__(self, x):
        #h = F.copy(x, gpu)                                                     # Copy to device
        h = F.relu(self.l1(x))
        h = self.b1(h)
        h = F.dropout(h, ratio=0.2)
        h = F.relu(self.l2(h))
        h = self.b2(h)
        h = F.max_pooling_2d(h, ksize=2, stride=2)
        h = F.dropout(h, ratio=0.2)
        # print('Level 1 Batch Done!')
        h = F.relu(self.fc1(h))
        return h

class Combined_FC(chainer.Chain):
    #initialized newly created object with arguments
    def __init__(self, n_out):
        super(Combined_FC, self).__init__()
        with self.init_scope():
            self.l1 = CNN()
            self.bfc = L.Bilinear(128,128,n_out)
        #arguments to call function
    def __call__(self,x1,x2):
            #h1 = F.copy(x1, gpu)
            #h2 = F.copy(x2, gpu)                                                      # Copy to device
            h1 = self.l1(x1)
            h2 = self.l1(x2)
            h3 = self.bfc(h1,h2)
            return F.relu(h3)
   
def main():
    train = load_data.load_train_data()
    #train = load_data_v2.load_train_data()
    class_labels = 31
    num_epochs = 100
    batchsize = 64
    #train_count = len(train)

    conv1 = CNN(class_labels)
    conv2 = CNN(class_labels)
    #model.to_gpu(gpu)                                                              # Copy model to device

    optimizer = chainer.optimizers.Adam()
    optimizer.setup(model)

    train_iter = chainer.iterators.SerialIterator(train, batchsize, repeat=True, shuffle=True)

    sum_loss = 0
    sum_accuracy = 0

    while train_iter.epoch < num_epochs:
        train_batch = train_iter.next()
        #print(train_batch)
        x_train, y_train = concat_examples(train_batch)
        #print('Length of x_train:')
        #print(len(y_train))

        y_train = y_train.astype(np.int32)                                       # Run in host
        #y_train = F.copy(y_train.astype(np.int32), gpu)                         # Copy to device

        x_train.tolist()

        list_x_train_1=[]
        list_x_train_2=[]
        #print(len(x_train[0]))
        for k in range (len(x_train)):
            x_train_1,x_train_2 = concat_examples(x_train[k])
            list_x_train_1.append(x_train_1)
            list_x_train_2.append(x_train_2) 
            
        #preimg
        print((list_x_train_1[0]))
        #deform image
        print((list_x_train_2[0]))
        
        y_pred = conv1()

        g = c.build_computational_graph(y_pred)
        with open('graph10.dot', 'w') as o:
            o.write(g.dump())

        # print(model.l20.l21.W)
        loss = F.softmax_cross_entropy(y_pred, y_train)
        accuracy = F.accuracy(y_pred, y_train)

        model.cleargrads()
        loss.backward()

        optimizer.update()

        sum_loss += float(loss.data) * len(y_train)
        sum_accuracy += float(accuracy.data) * len(y_train)

        if train_iter.is_new_epoch:
            print('epoch: ', train_iter.epoch)
            print('train mean loss: {}, accuracy: {}'.format(
                sum_loss / train_count, sum_accuracy / train_count))
            sum_loss = 0
            sum_accuracy = 0
            # print(train_iter.epoch)
            # print(loss.data)

    print('save the model')
    serializers.save_npz('flowsculpt.model', model)
    print('save the optimizer')
    serializers.save_npz('flowsculpt.state', optimizer)


if __name__ == '__main__':
    main()