from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import
from builtins import *  # NOQA
from future import standard_library
standard_library.install_aliases()


import argparse
import os
import sys
#sys.path.append(sys.path.pop(sys.path.index('/home/xylee/.local/lib/python3.6/site-packages')))


import gym
#gym.undo_logger_setup()
from gym import spaces
import gym.wrappers
import numpy as np
import cupy as cp
import gym_flowsculpt
import computational_graph
import copy
import chainer
import chainer.links as L
import chainer.functions as F
from chainer import training, function, link, initializers
from chainer import variable, cuda, serializers
from chainer import optimizers
from chainer.training import extensions

import chainerrl
print('Using Chainer from ', chainerrl.__file__)
from chainerrl.agents.dqn import DQN
from chainerrl import experiments
from chainerrl import explorers
from chainerrl import misc
from chainerrl import q_functions
from chainerrl import replay_buffer
from her import DQN_HER, DoubleDQN_HER

env = gym.make('Flowsculpt-v0')

class CNN(chainer.Chain): #takes an observation and returns an expected future return for each action the agent can take 

    def __init__(self):
        super().__init__()
        with self.init_scope():
            self.l1 = L.Convolution2D(1, 16, ksize = 8, stride = 2, pad = 0)
            self.b1 = L.BatchNormalization(16)
            self.l2 = L.Convolution2D(16, 32, ksize = 4, stride = 2,  pad = 2)
            #self.b2 = L.BatchNormalization(64)
            self.fc1 = L.Linear(128, 64) 
            #self.fc2 = L.Linear(128,n_actions)

    def __call__(self, x, test=False):
       
        #print('x in shape:',x.shape)
        #batch = x.shape[0]*x.shape[1]

        #x1 = x[0]
        #x = cp.reshape(x,(batch,1,100,12))
        #print('CNN in shape:',x.shape)
        #x1 = cp.asarray(x)
        #print('x1 shape:',x1.shape)
        h = F.copy(x, gpu)
        h = self.l1(h)
        h = F.max_pooling_2d(h, ksize=4, stride=4)
        h = F.relu(h)
        h = self.b1(h)
        #h = F.dropout(h, ratio = 0.2)
        h = self.l2(h)
        h = F.max_pooling_2d(h, ksize=2, stride=2)
        h = F.relu(h)
        #h = F.dropout(h, ratio=0.2)
        h = self.fc1(h)
        #h = self.fc2(h)
        #h = F.relu(self.fc2(h))
        return h
      

class Combined_FC(chainer.Chain):
    
    def __init__(self, n_actions):
        super(Combined_FC, self).__init__()
        with self.init_scope():
            self.l1 = CNN()
            #self.bfc = L.Bilinear(32,32,n_actions)
            self.l2 = L.Linear(64,n_actions)
        
    def __call__(self,x):
    #split x into x1, x5
        #print('x shape:',x.shape)
        batch = x.shape[0]
        x1 = []
        x5 = []
        #print(x)
        
        for i in range (x.shape[0]):
	        x1.append(x[i][0][0]) #initial image #first index when x shape = 32?
	        #print('x1 shape:',x1.shape)
	        #print(x1)
	        #print(x1) #ndarray
	        x5.append(x[i][1][0]) #final image
	        #print('x5 shape:',x5.shape)
	        #print(x5) #ndarray

        #print(len(x1))
        #print(x1[0].shape)
        #x1 = np.stack(x1[:])
        x1 = np.asarray(x1)
        x5 = np.asarray(x5)
        
        x1 = np.reshape(x1,(batch,1,100,12)).astype(np.float32)
        #print('x1 shape:',x1.shape)
        x5 = np.reshape(x5,(batch,1,100,12)).astype(np.float32)
        #print('x5 shape:',x5.shape)
        h1 = F.copy(x1, gpu)
        h2 = F.copy(x5, gpu)                                           # Copy to device
        h1 = self.l1(h1) #when copied to gpu, change x1 to h1
        h2 = self.l1(h2) #when copied to gpu, change x2 to h2
        h3 = h1*h2
        h3 = F.tanh(self.l2(h3))
        #print('h3:',h3.shape)
        return chainerrl.action_value.DiscreteActionValue(h3)

gpu = 0
chainer.cuda.get_device_from_id(gpu).use()

obs_size = env.observation_space.shape[0]
n_actions = env.action_space.n
q_func = Combined_FC(n_actions)
q_func.to_gpu(gpu)


optimizer = chainer.optimizers.RMSprop(lr=0.001, alpha=0.99, eps=1e-08)
optimizer.setup(q_func)


# Specify a replay buffer and its capacity.
replay_buffer = chainerrl.replay_buffer.ReplayBuffer(capacity=10 ** 6)
#replay_buffer =  chainerrl.replay_buffer.PrioritizedReplayBuffer(capacity=10**6)


# Set the discount factor that discounts future rewards.
gamma = 0.95

# Use epsilon-greedy for exploration
#explorer = chainerrl.explorers.ConstantEpsilonGreedy(
#    epsilon=0.3, random_action_func=env.action_space.sample)
explorer = chainerrl.explorers.LinearDecayEpsilonGreedy(
    start_epsilon=1, end_epsilon=0.01, decay_steps= 140000,  
    random_action_func=env.action_space.sample)

# Chainer only accepts numpy.float32 by default, specify
# a converter as a feature extractor function phi.
phi = lambda x: x.astype(np.float32, copy=False)

# Now create an agent that will interact with the environment.
#agent =chainerrl.agents.DoubleDQN 
agent =  DoubleDQN_HER(
    q_func, optimizer, replay_buffer, gamma, explorer,
    replay_start_size= 3500, update_interval=1,
    target_update_interval= 4000, phi=phi, minibatch_size= 128)

n_episodes = 80000
max_episode_len = 7
for i in range(1, n_episodes + 1):
    obs = env.reset()
    print('Episode:',i)
    reward = 0
    done = False
    R = 0  # return (sum of rewards)
    t = 0  # time step
    while not done and t < max_episode_len:
        action = agent.act_and_train(obs, reward)
        obs, reward, done, _ = env.step(action)
        R += reward
        t += 1
    if i % 5 == 0:
        with open('Statistics_DQN_Running_Target_HER.txt', 'a') as f:
            print('episode:', i,
                'R:', R,
                'statistics:', agent.get_statistics(), file = f)
    agent.stop_episode_and_train(obs, reward, done)
    if i % 200 == 0:
        agent.save('Partial_DQN_Running_Target_HER')
agent.save('Final_DQN_Running_Target_HER')
print('Finished.')
