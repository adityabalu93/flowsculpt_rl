import gym
import gym_flowsculpt

class RandomAgent(object):
    """The world's simplest agent!"""
    def __init__(self, action_space):
        self.action_space = action_space

    def act(self, observation, reward, done):
        return self.action_space.sample() #sample a random action from action space

if __name__ == '__main__':
   
    env = gym.make('Flowsculpt-v0')
    outdir = '/data/Bernard/flowsculpt_kin-20170918T032046Z-001/flowsculpt_kin/flowsculpt_rl/random-agent-results'
    env.seed(0)
    agent = RandomAgent(env.action_space)

    episode_count = 3
    reward = 0
    done = False

    for i in range(episode_count):
        print('Episode',i+1)
        ob = env.reset()
        action_counter= 0
        while action_counter < 7: #place max of 7 pillars
            action = agent.act(ob, reward, done)
            action_counter = action_counter + 1
            print(action)
            ob, reward, done, _ = env.step(action)
            if done:
                break
    env.close()

print('Success')

