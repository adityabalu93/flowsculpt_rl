#this should be the file in gym-foo/gym_foo/envs/flowsculpt_env.py
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import random
import numpy as np
import matlab.engine
import gym
from gym import error, spaces, utils 
from gym.utils import seeding 
from skimage.measure import compare_ssim as ssim

np.set_printoptions(threshold=np.inf)

def initial_flow(): #generates initial flow
    top = np.zeros((35,12))
    mid = np.ones((30,12))
    bottom = np.zeros((35,12))
    flow = np.concatenate((top, mid, bottom), axis=0)
    #print(flow.shape)
    #print(flow)
    return flow

def final_flow(): #generates random final flow
    eng = matlab.engine.start_matlab() 
    final_img = np.ones((100,12))
    pillar_seq = []
    for i in range (7): #final image is based off flow sculpted based on 4 pillar
        pillar = random.randint(1,31)
        pillar_seq.append(pillar)
    pillar_seq = matlab.int8(pillar_seq)
    #print(pillar_seq)
    final_img[:,:] = eng.Pillar_program_regen2_12100(pillar_seq)
    #print(final_img.shape)
    return final_img, pillar_seq

def fixed_final_flow(): #generates deterministic final flow
    eng = matlab.engine.start_matlab() 
    final_img = np.ones((100,12))
    pillar_seq = [19, 2, 28, 15, 7 ,17, 22] 
    pillar_seq = matlab.int8(pillar_seq)
    #print(pillar_seq)
    final_img[:,:] = eng.Pillar_program_regen2_12100(pillar_seq)
    #print(final_img.shape)
    #print(final_img)
    return final_img, pillar_seq

def getPMR(deform, goal):

    diff = deform - goal
    #print(deform)
    #print('===============================================================')
    #print(goal)
    #print('===============================================================')
    #diff = np.where(diff > 0, 1, 0) #convert everything to binary
    #print(diff)
    PMR = np.sum(abs(diff))/(deform.size)
    PMR = 1-PMR

    return PMR

class FlowEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self):
        self.action_space = spaces.Discrete(31) # 31 possible actions #need to change to array 0 to 31
        self.observation_space = spaces.Box(low=0, high=1, shape=(2, 1, 100, 12))
        self.epscounter = 0
        self.suc_counter = 0
        self.state = self.reset()
        self.true_pillars = 0
        #print(self.state.shape) #(2,1,100,12)
        self.goal = self.state[1][0]
        self.PMR = 0
        self.SSIM = 0
        #print('Gym Make Goal :',self.goal)
        #print('self.goal',self.goal.shape) #(100,12)
        self.pillars = []
        self.endgame = 0.85 # win condition when SSIM is greater than 0.9

    def reset(self):
        print('--------------------Resetting-----------------------')
        pre_img = initial_flow() 
        final_img, true_seq = final_flow()
        #print('Final image:', final_img.shape) # (100,12)
        #print('True Seq:', (true_seq)) #length of 1
        self.true_pillars = true_seq
        #print(self.true_pillars)
        #print('final image',final_img)
        self.state = np.array([[pre_img],[final_img]]) #generate initial states of pre and def image pairs
        self.goal = self.state[1][0]
        #print('Self.Goal Reset',self.goal)
        self.suc_counter = 0
        self.epscounter = self.epscounter + 1
        self.pillars = []
        #print('reset state:', self.state.shape) #(2, 1, 100, 12)
        #print(state)
        return np.array(self.state)

    #step
    def step(self,action):

        eng = matlab.engine.start_matlab() 

        state = self.state # current state of flow_gen
        if action == 0:
            print('Generated 0, selecting another random pillar')
            action = random.randint(1,31)
        else:
            action = action
        pillar = action
        self.pillars.append(pillar)
        matlab_pillars = matlab.int8(self.pillars)
        print('Pillars: ',matlab_pillars)

        def_flow = np.zeros((100,12))
        def_flow[:,:]  = eng.Pillar_program_regen2_12100(matlab_pillars)
        final_img = self.goal
        #print('Step Goal should be same as Self.Goal Reset',final_img)
        #print('final_img',final_img)
        self.state = np.array([[def_flow],[final_img]]) #update state 
        #print('step state:', self.state.shape) #(2, 1, 100, 12)
        self.suc_counter += 1
        reward = self.get_neg_reward_PMR()
        #reward = self.get_reward_SSIM()

        done = False
        
        if self.PMR > self.endgame: 
            with open('SuccessRate_DQN_Running_Target_HER.txt', 'a') as g:
                print('Episode: ', self.epscounter, ' Steps to sucess: ', self.suc_counter , 
                	  'Predicted: ', matlab_pillars, ' True: ', self.true_pillars,  file = g)
            #to save created flow
            #created_shape = self.state
            #plt.imsave('created_flow_DQN_PMR_v2_2_%f.png'%self.PMR, np.array(created_shape).reshape(100,12), cmap=cm.gray)
            print(self.state)
            done = True
        '''
        if self.SSIM > self.endgame: 
            with open('SuccessRate_DQN_HER_PRB_SSIM_1.txt', 'a') as g:
                print('Episode: ', self.epscounter, ' Steps to sucess: ', self.suc_counter , 
                	  'Predicted: ', matlab_pillars, ' True: ', self.true_pillars,  file = g)
            created_shape = self.state
            #to save created flow
            #plt.imsave('created_flow_DQN_PMR_v2_2_%f.png'%self.SSIM, np.array(created_shape).reshape(100,12), cmap=cm.gray)
            print(self.state)
            done = True

        '''
        return np.array(self.state),reward,done,{} #what is the last curly brace


    def get_reward_SSIM(self):
        #strucutral similarity index
        state = self.state
        #print(state.shape)
        #print(state[0][0])
        #print(state[1][0])
        self.SSIM = ssim(state[0][0],state[1][0]) #metric between 0 to 1 
        reward = -(1-self.SSIM)
        print ('SSIM: ', self.SSIM, ',Reward: ', reward)
        return reward

    def get_neg_reward_PMR(self):
        state = self.state
        cur_flow = state[0][0]
        goal = state[1][0]
        self.PMR = getPMR(cur_flow, goal)
        reward = -(1-self.PMR)
        print ('PMR: ', self.PMR, ',Reward: ', reward)

        return reward

   
def main():
    print('import success')

if __name__ == '__main__':
    main()

