#make_env line 170 and line 248
#GPU version
"""An example of training DQN against OpenAI Gym Envs.
This script is an example of training a DQN agent against OpenAI Gym envs.
Both discrete and continuous action spaces are supported. For continuous action
spaces, A NAF (Normalized Advantage Function) is used to approximate Q-values.
To solve CartPole-v0, run:
    python train_dqn_gym.py --env CartPole-v0
To solve Pendulum-v0, run:
    python train_dqn_gym.py --env Pendulum-v0
"""
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import
from builtins import *  # NOQA
from future import standard_library
standard_library.install_aliases()

import argparse
import os
import sys

import gym
gym.undo_logger_setup()
from gym import spaces
import gym.wrappers
import numpy as np
import cupy as cp
import gym_flowsculpt
import computational_graph

import chainer
import chainer.links as L
import chainer.functions as F
from chainer import training, function, link, initializers
from chainer import variable, cuda, serializers
from chainer import optimizers
from chainer.training import extensions

import chainerrl
from chainerrl.agents.dqn import DQN
from chainerrl import experiments
from chainerrl import explorers
from chainerrl import misc
from chainerrl import q_functions
from chainerrl import replay_buffer

gpu = 0
chainer.cuda.get_device_from_id(gpu).use()
#DNN to approximate Q function
class CNN(chainer.Chain): #takes an observation and returns an expected future return for each action the agent can take 

    def __init__(self):
        super().__init__()
        with self.init_scope():
            self.l1 = L.Convolution2D(None, 32, ksize = 4, stride = 2, pad = 1)
            self.b1 = L.BatchNormalization(32)
            self.l2 = L.Convolution2D(None, 64, ksize = 4, stride = 1,  pad = 1)
            self.b2 = L.BatchNormalization(64)
            self.fc1 = L.Linear(None, 128)

    def __call__(self, x, test=False):
        """
        Args:
            x (ndarray or chainer.Variable): An observation
            test (bool): a flag indicating whether it is in test mode
        """
        h = F.copy(x, gpu)
        h = F.relu(self.l1(x))
        h = self.b1(h)
        h = F.dropout(h, ratio = 0.2)
        h = F.relu(self.l2(h))
        h = self.b2(h)
        h = F.max_pooling_2d(h, ksize=2, stride=2)
        h = F.dropout(h, ratio=0.2)
        h = F.relu(self.fc1(h))
        return h

class Combined_FC(chainer.Chain):
    #initialized newly created object with arguments
    def __init__(self, n_actions):
        super(Combined_FC, self).__init__()
        with self.init_scope():
            self.l1 = CNN()
            self.bfc = L.Bilinear(128,128,n_actions)
        #arguments to call function
    def __call__(self,x):
    #split x into x1, x5
        print('x shape:',x.shape)
        #print(x)
        x1 = x[0][0][0] #initial image #first index when x shape = 32?
        print('x1 shape:',x1.shape)
#        print(x1) #ndarray
        x5 = x[0][1][0] #final image
        print('x5 shape:',x5.shape)
 #       print(x5) #ndarray
        x1 = cp.asarray(x1)
        x1 = cp.reshape(x1,(1,1,100,12)).astype('float32')
        x5 = cp.asarray(x5)
        x5 = cp.reshape(x5,(1,1,100,12)).astype('float32')
        h1 = F.copy(x1, gpu)
        h2 = F.copy(x5, gpu)                                           # Copy to device
        h1 = self.l1(h1) #when copied to gpu, change x1 to h1
        h2 = self.l1(h2) #when copied to gpu, change x2 to h2
        h3 = self.bfc(h1,h2)
        h3 = F.relu(h3)
        return chainerrl.action_value.DiscreteActionValue(h3)


def main():
    import logging
    #logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--outdir', type=str, default='chainer_dqn_out')
    parser.add_argument('--env', type=str, default='Flowsculpt-v0')
    parser.add_argument('--seed', type=int, default=None)
    parser.add_argument('--gpu', type=int, default=0)
    parser.add_argument('--final-exploration-steps',
                        type=int, default=10 ** 4)
    parser.add_argument('--start-epsilon', type=float, default=1.0)
    parser.add_argument('--end-epsilon', type=float, default=0.1)
    parser.add_argument('--demo', action='store_true', default=False)
    parser.add_argument('--load', type=str, default=None)
    parser.add_argument('--steps', type=int, default=10 ** 5)
    parser.add_argument('--prioritized-replay', action='store_true')
    parser.add_argument('--episodic-replay', action='store_true')
    parser.add_argument('--replay-start-size', type=int, default=1000)
    parser.add_argument('--target-update-interval', type=int, default=10 ** 2)
    parser.add_argument('--target-update-method', type=str, default='hard')
    parser.add_argument('--soft-update-tau', type=float, default=1e-2)
    parser.add_argument('--update-interval', type=int, default=1)
    parser.add_argument('--eval-n-runs', type=int, default=100)
    parser.add_argument('--eval-interval', type=int, default=10 ** 4)
    parser.add_argument('--n-hidden-channels', type=int, default=100)
    parser.add_argument('--n-hidden-layers', type=int, default=2)
    parser.add_argument('--gamma', type=float, default=0.99)
    parser.add_argument('--minibatch-size', type=int, default=None)
    parser.add_argument('--render-train', action='store_true')
    parser.add_argument('--render-eval', action='store_true')
    parser.add_argument('--monitor', action='store_true')
    parser.add_argument('--reward-scale-factor', type=float, default=1e-3)
    args = parser.parse_args()

    args.outdir = experiments.prepare_output_dir(
        args, args.outdir, argv=sys.argv)
    print('Output files are saved in {}'.format(args.outdir))

    if args.seed is not None:
        misc.set_random_seed(args.seed)

    def clip_action_filter(a): #if interval is [0,1], values < 0 becomes 0 and > 1 becomes 1
        return cp.clip(a, action_space.low, action_space.high)

    def make_env(for_eval):
        env = gym.make(args.env)
        if args.monitor:
            env = gym.wrappers.Monitor(env, args.outdir)
        if isinstance(env.action_space, spaces.Box):
            misc.env_modifiers.make_action_filtered(env, clip_action_filter)
        if not for_eval:
            misc.env_modifiers.make_reward_filtered(
                env, lambda x: x * args.reward_scale_factor)
        if ((args.render_eval and for_eval) or
                (args.render_train and not for_eval)):
            misc.env_modifiers.make_rendered(env)
        return env

    env = make_env(for_eval=False)
    print('Environment not for evaluation')
    timestep_limit = 7
    # timestep_limit = env.spec.tags.get(
    #     'wrapper_config.TimeLimit.max_episode_steps')
    #print(timestep_limit)
    obs_space = env.observation_space
    obs_size = obs_space.low.size
    print('obs_size:',obs_size)
    print('obs_shape',obs_space.shape)
    action_space = env.action_space

    if isinstance(action_space, spaces.Box): #action space is spaces.Discrete, move on to ELSE
        action_size = action_space.low.size
        # Use NAF to apply DQN to continuous action spaces
        q_func = q_functions.FCQuadraticStateQFunction(
            obs_size, action_size,
            n_hidden_channels=args.n_hidden_channels,
            n_hidden_layers=args.n_hidden_layers,
            action_space=action_space)
        # Use the Ornstein-Uhlenbeck process for exploration
        ou_sigma = (action_space.high - action_space.low) * 0.2
        explorer = explorers.AdditiveOU(sigma=ou_sigma) 
    else:
        n_actions = action_space.n
        q_func = Combined_FC(n_actions)
        # Use epsilon-greedy for exploration/ can try contant epsilon greedy
        explorer = explorers.LinearDecayEpsilonGreedy(
            args.start_epsilon, args.end_epsilon, args.final_exploration_steps,
            action_space.sample)

        q_func.to_gpu(gpu)

    # computational_graph.draw_computational_graph(
    #     [q_func(cp.zeros((1,2,1,100,12), dtype=cp.float32))],
    #     os.path.join(args.outdir, 'model'))
    computational_graph.draw_computational_graph(
        [q_func(cp.zeros((32,2,1,100,12), dtype=cp.float32))],
        os.path.join(args.outdir, 'model'))


    optimizer = optimizers.SGD() #In APN, SGD yield better result
    optimizer.setup(q_func)

    rbuf_capacity = 5 * 10 ** 5
    if args.episodic_replay:
        if args.minibatch_size is None:
            args.minibatch_size = 4
        if args.prioritized_replay:
            betasteps = (args.steps - args.replay_start_size) \
                // args.update_interval
            rbuf = replay_buffer.PrioritizedEpisodicReplayBuffer(
                rbuf_capacity, betasteps=betasteps)
        else:
            rbuf = replay_buffer.EpisodicReplayBuffer(rbuf_capacity)
    else:
        if args.minibatch_size is None:
            args.minibatch_size = 32
        if args.prioritized_replay:
            betasteps = (args.steps - args.replay_start_size) \
                // args.update_interval
            rbuf = replay_buffer.PrioritizedReplayBuffer(
                rbuf_capacity, betasteps=betasteps)
        else:
            rbuf = replay_buffer.ReplayBuffer(rbuf_capacity)

    def phi(obs):
    	#print('obs:',obs)
    	print('obs shape:',obs.shape)
    	return obs.astype(cp.float32)

    agent = DQN(q_func, optimizer, rbuf, gpu=args.gpu, gamma=args.gamma,
                explorer=explorer, replay_start_size=args.replay_start_size,
                target_update_interval=args.target_update_interval,
                update_interval=args.update_interval,
                phi=phi, minibatch_size=args.minibatch_size,
                target_update_method=args.target_update_method,
                soft_update_tau=args.soft_update_tau,
                episodic_update=args.episodic_replay, episodic_update_len=16)

    if args.load:
        agent.load(args.load)

    eval_env = make_env(for_eval=True) #wraps gym.make function

    if args.demo: #default is false, proceed to ELSE
        eval_stats = experiments.eval_performance( #Run multiple evaluation episodes and return statistics.
            env=eval_env,
            agent=agent,
            n_runs=args.eval_n_runs,
            max_episode_len=timestep_limit)
        print('n_runs: {} mean: {} median: {} stdev {}'.format(
            args.eval_n_runs, eval_stats['mean'], eval_stats['median'],
            eval_stats['stdev']))
    else:
        experiments.train_agent_with_evaluation( #Train an agent while regularly evaluating it
            agent=agent, env=env, steps=args.steps,
            eval_n_runs=args.eval_n_runs, eval_interval=args.eval_interval,
            outdir=args.outdir, eval_env=eval_env,
            max_episode_len=timestep_limit)



if __name__ == '__main__':
    main()
