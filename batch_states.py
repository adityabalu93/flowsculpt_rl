import numpy as np
def batch_states(states, xp, phi):
    """The default method for making batch of observations.

    Args:
        states (list): list of observations from an environment.
        xp (module): numpy or cupy
        phi (callable): Feature extractor applied to observations

    Return:
        the object which will be given as input to the model.
    """
    print('Entering batch states')
    #print('length of states:',len(states))
    #print('shape of state:',states[0].shape)
    states = [phi(s) for s in states]
    #print('States after phi:', len(states)) #length is 1, 4 after experience replay begin

    # if (len(states)==1):
    #     print('States after phi:', states[0].shape)
    # else:
    #     #states nominally are (1,1,100,12) but sometimes it is becoming (1,100,12) then it fails
    #     #need to find out why is it becoming (1,100,12)

    #     print('States 0 after phi:', states[0].shape)
    #     print('States 1 after phi:', states[1].shape)
    #     print('States 2 after phi:', states[2].shape)
    #     print('States 3 after phi:', states[3].shape)

    # for fixed states
    if states[0].size == 1200:
        for i in range(len(states)):
            states[i] = np.reshape(states[i],(1,1,100,12))
    # for running target
    else:
        for i in range(len(states)):
            states[i] = np.reshape(states[i],(2,1,100,12))

    states = xp.asarray(states)
    
    print('       ')
    return states

