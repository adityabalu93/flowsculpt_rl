#working script of agent with random explore/exploit, need to add part to update weights
import gym
import gym_flowsculpt
import numpy as np
import APN
from APN import Combined_FC, CNN
import chainer
from chainer import serializers

class Agent(object):
    """The world's simplest agent!"""
    def __init__(self, action_space):
        self.action_space = action_space


    def act(self, observation, reward, done):
        num = np.random.randint(0,100)
        if (num%2) == 0: # decide to explore or exploit
            #exploring
            print('Exploring')
            pillar_ind = self.action_space.sample() #sample a random action from action space
            if pillar_ind == 0: # to avoid sampling 0 
                pillar_ind = pillar_ind + 1
            return pillar_ind
            
        else:
            print('Exploiting')
            #exploit previously trained CNN model
            #call model to predict
            x1 = observation[0][0] #initial image
            x2 = observation[1][0] #final image
            x1_test = np.asarray(x1)
            x1_test = np.reshape(x1,(1,1,100,12)).astype('float32')
            x2_test = np.asarray(x2)
            x2_test = np.reshape(x2,(1,1,100,12)).astype('float32')
            #print(x1_test[0][0])
            #print(x2_test[0][0])
            class_labels = 32
            model = Combined_FC(class_labels)
            # Load the saved paremeters into the instance
            serializers.load_npz('flowsculpt.model', model)
            y_pred = model(x1_test,x2_test)
            y_pred = y_pred.data
            pillar_ind= y_pred.argmax(axis=1) #predict pillar
            pillar_ind_int = np.asscalar(pillar_ind)
            return pillar_ind_int


    
if __name__ == '__main__':
   
    env = gym.make('Flowsculpt-v0')
    outdir = '/data/Bernard/flowsculpt_kin-20170918T032046Z-001/flowsculpt_kin/flowsculpt_rl/random-agent-results'
    env.seed(0)
    agent = Agent(env.action_space)

    episode_count = 3
    reward = 0
    done = False

    for i in range(episode_count):
        print('Episode',i+1)
        ob = env.reset()
        action_counter= 0
        while action_counter < 7: #place max of 7 pillars
            action = agent.act(ob, reward, done)
            action_counter = action_counter + 1
            print(action)
            ob, reward, done, _ = env.step(action)
            if done:
                break
    env.close()

print('Success')
